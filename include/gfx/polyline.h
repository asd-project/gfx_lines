//---------------------------------------------------------------------------

#pragma once

#ifndef GFX_LINES_H
#define GFX_LINES_H

//---------------------------------------------------------------------------

#include <gfx/line.h>

//---------------------------------------------------------------------------

namespace asd
{
    namespace gfx
    {
        template <class T, class Model>
        class polyline
        {
        public:
            using point_model_type = Model;
            using point_type = math::point<T, Model>;
            using normal_type = math::point<math::fp<T>, Model>;
            using range_type = math::range<size_t>;

            const auto & points() const noexcept {
                return _points;
            }

            template <class It, useif<std::is_same_v<point_type, typename std::iterator_traits<It>::value_type>>>
            size_t add(It first, It last) {
                auto count = std::distance(first, last);
                auto & range = _ranges.emplace_back(_points.size(), _points.size() + count);

                _points.insert(_points.end(), first, last);
                _normals.insert(_normals.end(), count, {});

                make_line(_points.begin(), _points.end(), _normals.begin(), range.min, range.max);

                return _ranges.size() - 1;
            }

            template <class It, useif<std::is_same_v<point_type, typename std::iterator_traits<It>::value_type>>>
            size_t insert(size_t range_index, It first, It last) {
                if (range_index >= _ranges.size()) {
                    return add(first, last);
                }

                auto count = std::distance(first, last);
                auto start = _ranges[range_index].min;

                for (size_t i = range_index; i < _ranges.size(); ++i) {
                    _ranges[i] += count;
                }

                auto & range = *_ranges.emplace(_ranges.begin() + range_index, start, start + count);

                _points.insert(_points.begin() + range.min, first, last);
                _normals.insert(_normals.begin() + range.min, count, {});

                make_line(_points.begin(), _points.end(), _normals.begin(), range.min, range.max);

                return range_index;
            }

            template <class It, useif<std::is_same_v<point_type, typename std::iterator_traits<It>::value_type>>>
            void replace(size_t range_index, It first, It last) {
                auto & range = _ranges[range_index];
                auto current_count = range.size();
                auto count = std::distance(first, last);

                std::copy(first, first + math::min(current_count, count), _points.begin() + range.min);

                if (count != current_count) {
                    for (size_t i = range_index + 1; i < _ranges.size(); ++i) {
                        _ranges[i] += count - current_count;
                    }

                    if (count < current_count) {
                        _points.erase(_points.begin() + count, _points.begin() + current_count);
                        _normals.erase(_normals.begin() + count, _normals.begin() + current_count);
                    } else {
                        _normals.insert(_points.begin() + range.max, first + current_count, last);
                        _normals.insert(_normals.begin() + range.max, count - current_count, {});
                    }

                    range.max = range.min + count;
                }

                make_line(_points.begin(), _points.end(), _normals.begin(), range.min, range.max);
            }

            void remove(size_t range_index) {
                auto range = _ranges[range_index];
                _ranges.erase(_ranges.begin() + range_index);

                _points.erase(_points.begin() + range.min, _points.begin() + range.max);
                _normals.erase(_normals.begin() + range.min, _normals.begin() + range.max);

                make_line(_points.begin(), _points.end(), _normals.begin(), range.min, range.min);
            }

        private:
            std::vector<range_type> _ranges;
            std::vector<point_type> _points;
            std::vector<normal_type> _normals;
        };
    }
}

//---------------------------------------------------------------------------
#endif
