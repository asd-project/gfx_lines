//---------------------------------------------------------------------------

#pragma once

#ifndef GFX_LINE_H
#define GFX_LINE_H

//---------------------------------------------------------------------------

#include <vector>
#include <variant>

#include <math/point.h>
#include <math/range.h>

#include <gfx/vertex_data.h>

//---------------------------------------------------------------------------

namespace asd
{
    namespace gfx
    {
        template <class T, class Model>
        struct line_vertex
        {
            math::point<T, Model> position;
            math::float_point_xyz texcoord;
        };

        static_assert(std::is_standard_layout_v<line_vertex<float, math::point_model_xy>>);
        static_assert(sizeof(line_vertex<float, math::point_model_xy>) == 20);
        static_assert(sizeof(line_vertex<float, math::point_model_xyz>) == 24);

        template <class T, class Model>
        struct line_mesh
        {
            std::vector<line_vertex<T, Model>> vertices;
            std::vector<u32> indices;
        };

        template <class T, class Model>
        struct line_anchor
        {
            operator const math::point<T, Model> & () const {
                return point;
            }

            operator math::point<T, Model> & () {
                return point;
            }

            math::point<T, Model> point;
            u32 vertices_offset = 0;
            u32 indices_offset = 0;
            float offset = 0.0f;
        };

        struct line_join_miter {};
        struct line_join_bevel {};

        struct line_join_round
        {
            constexpr line_join_round(float division_coeff = 2.0f) :
                division_coeff(division_coeff) {}

            /**
             * @brief Allows to tweak join arc quality
             * Actual amount of points depends on the arc size
             * radius = 0.5 * thickness
             * divisions = math::ceil(2.0 * division_coeff * radius * angle / math::pi)
             */
            float division_coeff;
        };

        struct line_cap_flat {};
        struct line_cap_square {};

        struct line_cap_round
        {
            constexpr line_cap_round(float division_coeff = 2.0f) :
                division_coeff(division_coeff) {}

            /**
             * @brief Allows to tweak cap arc quality
             * Actual amount of points depends on the arc size
             * radius = 0.5 * thickness
             * divisions = math::ceil(2.0 * division_coeff * radius)
             */        
            float division_coeff;
        };

        template <class ... T>
        struct line_join_dynamic
        {
            line_join_dynamic(const line_join_dynamic & v) = default;
            line_join_dynamic(line_join_dynamic && v) = default;

            line_join_dynamic(const std::variant<T...> & v) : variant(v) {}
            line_join_dynamic(std::variant<T...> && v) : variant(std::move(v)) {}

            std::variant<T...> variant;
        };

        template <class ... T>
        struct line_cap_dynamic
        {
            line_cap_dynamic(const line_cap_dynamic & v) = default;
            line_cap_dynamic(line_cap_dynamic && v) = default;

            line_cap_dynamic(const std::variant<T...> & v) : variant(v) {}
            line_cap_dynamic(std::variant<T...> && v) : variant(std::move(v)) {}

            std::variant<T...> variant;
        };

//---------------------------------------------------------------------------

        template <class T, class Model>
        void start_cap(line_cap_flat, line_mesh<T, Model> & output, line_anchor<T, Model> & anchor, const line_anchor<T, Model> & to, float thickness);

        template <class T, class Model>
        void end_cap(line_cap_flat, line_mesh<T, Model> & output, line_anchor<T, Model> & anchor, const math::point<T, Model> & v, float start_offset, float thickness);

        template <class T, class Model>
        void line_point(const line_cap_round & cap, line_mesh<T, Model> & output, line_anchor<T, Model> & anchor, float thickness);

        template <class T, class Model>
        void start_cap(const line_cap_round & cap, line_mesh<T, Model> & output, line_anchor<T, Model> & anchor, const line_anchor<T, Model> & to, float thickness);

        template <class T, class Model>
        void end_cap(const line_cap_round & cap, line_mesh<T, Model> & output, line_anchor<T, Model> & anchor, const math::point<T, Model> & v, float start_offset, float thickness);

//---------------------------------------------------------------------------

        template <class T, class Model, class It>
        void append_line(line_join_miter, line_mesh<T, Model> & output, It it, It end, float thickness);

        template <class T, class Model, class It>
        void append_line(line_join_bevel, line_mesh<T, Model> & output, It it, It end, float thickness);

        template <class T, class Model, class It>
        void append_line(const line_join_round & join, line_mesh<T, Model> & output, It it, It end, float thickness);

        template <class T, class Model, class It, class ... V>
        void append_line(const line_join_dynamic<V...> & join, line_mesh<T, Model> & output, It begin, It end, float thickness) {
            std::visit([&](const auto & join) { append_line(join, output, begin, end, thickness); }, join.variant);
        }

//---------------------------------------------------------------------------

        template <class T, class Model>
        class line
        {
        public:
            using point_model_type = Model;
            using point_type = math::point<T, Model>;
            using line_anchor_type = line_anchor<T, Model>;

            const auto & vertices() const noexcept {
                return _output.vertices;
            }

            const auto & indices() const noexcept {
                return _output.indices;
            }

            bool can_place(const point_type & pt, float thickness) {
                if (_anchors.empty()) {
                    return true;
                }

                if (_anchors.size() == 1) {
                    return math::square_distance(pt, _anchors.back().point) >= thickness * thickness;
                }

                return math::ray_square_distance(pt, _anchors[_anchors.size() - 1].point, _anchors[_anchors.size() - 2].point) >= thickness * thickness;
            }

            template <class It, class Join = line_join_miter, class Cap = line_cap_round>
            void append(It first, It last, float thickness, const Join & join = Join{}, const Cap & cap = Cap{}) {
                if (first == last) {
                    return;
                }

                auto current_size = _anchors.size();

                std::transform(first, last, std::back_inserter(_anchors), [](auto & point) {
                    return line_anchor_type{point};
                });

                if (_anchors.size() == 1) {
                    line_point(cap, _output, _anchors[0], thickness);
                    return;
                }
            
                if (current_size == 0) {
                    start_cap(cap, _output, _anchors[0], _anchors[1], thickness);
                    current_size = 1;
                } else if (current_size == 1) {
                    _output.vertices.clear();
                    _output.indices.clear();
                    start_cap(cap, _output, _anchors[0], _anchors[1], thickness);
                } else {
                    // fix last point
                    line_anchor_type & last_anchor = _anchors[current_size - 1];
                    _output.vertices.erase(_output.vertices.begin() + last_anchor.vertices_offset, _output.vertices.end());
                    _output.indices.erase(_output.indices.begin() + last_anchor.indices_offset, _output.indices.end());
                }

                if (current_size >= 2) {
                    append_line(join, _output, _anchors.begin() + current_size - 2, _anchors.end(), thickness);
                }

                auto & last_anchor = _anchors[_anchors.size() - 1];
                auto & prev_anchor = _anchors[_anchors.size() - 2];
                end_cap(cap, _output, last_anchor, last_anchor.point - prev_anchor.point, prev_anchor.offset, thickness);
            }

        private:
            std::vector<line_anchor_type> _anchors;
            line_mesh<T, Model> _output;
        };

//---------------------------------------------------------------------------

        template <class T, class Model>
        void append_section(line_mesh<T, Model> & output, line_anchor<T, Model> & anchor, const math::point<T, Model> & normal, float offset, T thickness) {
            anchor.vertices_offset = output.vertices.size();
            anchor.indices_offset = output.indices.size();
            anchor.offset = offset;

            auto n = thickness * normal;

            output.vertices.push_back({anchor.point + n, {offset, +1.0f, thickness}});
            output.vertices.push_back({anchor.point - n, {offset, -1.0f, thickness}});

            output.indices.push_back(anchor.vertices_offset - 2);
            output.indices.push_back(anchor.vertices_offset + 1);
            output.indices.push_back(anchor.vertices_offset - 1);
            output.indices.push_back(anchor.vertices_offset - 2);
            output.indices.push_back(anchor.vertices_offset + 0);
            output.indices.push_back(anchor.vertices_offset + 1);
        }

        template <class T, class Model>
        void append_bevel(line_mesh<T, Model> & output, line_anchor<T, Model> & anchor, const math::point<T, Model> & a, const math::point<T, Model> & b, T dot, float offset, float thickness) {
            anchor.vertices_offset = output.vertices.size();
            anchor.indices_offset = output.indices.size();
            anchor.offset = offset;

            auto n = thickness * math::tangent_normal(a, -b);
            output.vertices.push_back({anchor.point + b * thickness / dot, {offset, +1.0f, thickness}});
            output.vertices.push_back({anchor.point + a * thickness / dot, {offset, +1.0f, thickness}});
            output.vertices.push_back({anchor.point + n, {offset, -1.0f, thickness}});

// prev to bevel
            output.indices.push_back(anchor.vertices_offset - 2);
            output.indices.push_back(anchor.vertices_offset + 2);
            output.indices.push_back(anchor.vertices_offset - 1);
            output.indices.push_back(anchor.vertices_offset - 2);
            output.indices.push_back(anchor.vertices_offset + 0);
            output.indices.push_back(anchor.vertices_offset + 2);

// bevel
            output.indices.push_back(anchor.vertices_offset + 0);
            output.indices.push_back(anchor.vertices_offset + 1);
            output.indices.push_back(anchor.vertices_offset + 2);
        }

        template <class T, class Model>
        void start_cap(line_cap_flat, line_mesh<T, Model> & output, line_anchor<T, Model> & anchor, const line_anchor<T, Model> & to, float thickness) {
            auto normal = thickness * math::line_normal(math::normalize(to.point - anchor.point));
            
            output.vertices.push_back({anchor.point + normal, {0.0f, +1.0f, thickness}});
            output.vertices.push_back({anchor.point - normal, {0.0f, -1.0f, thickness}});
        }

        template <class T, class Model>
        void end_cap(line_cap_flat, line_mesh<T, Model> & output, line_anchor<T, Model> & anchor, const math::point<T, Model> & v, float start_offset, float thickness) {
            auto normal = math::line_normal(math::normalize(v));
            append_section(output, anchor, normal, start_offset + math::length(v), thickness);
        }

        template <class T, class Model>
        void line_point(const line_cap_round & cap, line_mesh<T, Model> & output, line_anchor<T, Model> & anchor, float thickness) {
            anchor.vertices_offset = output.vertices.size();
            anchor.indices_offset = output.indices.size();

            output.vertices.push_back({anchor.point, {0.0f, 0.0f, thickness}});

            auto divisions = math::ceil<int>(2.0f * cap.division_coeff * thickness);
            output.vertices.push_back({anchor.point + math::point<T, Model>{thickness, 0}, {0.0f, 1.0f, thickness}});

            for (int i = 1; i <= divisions; ++i) {
                auto angle = 2.0f * math::pi * i / divisions;
                output.vertices.push_back({anchor.point + thickness * math::fast::direction(angle), {0.0f, 1.0f, thickness}});

                output.indices.push_back(anchor.vertices_offset);
                output.indices.push_back(anchor.vertices_offset + i);
                output.indices.push_back(anchor.vertices_offset + i - 1);
            }

            output.indices.push_back(anchor.vertices_offset);
            output.indices.push_back(anchor.vertices_offset + 1);
            output.indices.push_back(anchor.vertices_offset + divisions);
        }

        template <class T, class Model>
        void start_cap(const line_cap_round & cap, line_mesh<T, Model> & output, line_anchor<T, Model> & anchor, const line_anchor<T, Model> & to, float thickness) {
            auto normal = math::line_normal(math::fast::normalize(to.point - anchor.point));
            auto antinormal = -normal;
            auto middle = math::avg(anchor.point, to.point); // required for transition from the 3-point segment to the 2-point (to avoid junction at the line cap)
            auto middle_distance = math::distance(anchor.point, middle);

            output.vertices.push_back({anchor.point + thickness * normal, {0.0f, 1.0f, thickness}});

            auto divisions = math::ceil<int>(cap.division_coeff * thickness);

            for (int i = 0; i < divisions; ++i) {
                output.vertices.push_back({anchor.point + thickness * math::fast::rotate(normal, math::pi * float(i + 1) / (divisions + 1)), {0.0f, 1.0f, thickness}});

                output.indices.push_back(divisions + 1);
                output.indices.push_back(i + 1);
                output.indices.push_back(i);
            }

            output.indices.push_back(divisions + 1);
            output.indices.push_back(divisions + 2);
            output.indices.push_back(divisions);
            
            output.vertices.push_back({anchor.point, {0.0f, 0.0f, thickness}});
            output.vertices.push_back({anchor.point + thickness * antinormal, {0.0f, 1.0f, thickness}});
            output.vertices.push_back({anchor.point + thickness * antinormal, {0.0f, -1.0f, thickness}});

            output.vertices.push_back({middle + thickness * normal, {middle_distance, +1.0f, thickness}});
            output.vertices.push_back({middle + thickness * antinormal, {middle_distance, -1.0f, thickness}});

            output.indices.push_back(divisions + 1);
            output.indices.push_back(0);
            output.indices.push_back(divisions + 4);

            output.indices.push_back(divisions + 1);
            output.indices.push_back(divisions + 5);
            output.indices.push_back(divisions + 3);

            output.indices.push_back(divisions + 1);
            output.indices.push_back(divisions + 4);
            output.indices.push_back(divisions + 5);
        }

        template <class T, class Model>
        void end_cap(const line_cap_round & cap, line_mesh<T, Model> & output, line_anchor<T, Model> & anchor, const math::point<T, Model> & v, float start_offset, float thickness) {
            auto offset = math::length(v);

            anchor.vertices_offset = output.vertices.size();
            anchor.indices_offset = output.indices.size();
            anchor.offset = start_offset + offset;

            auto normal = thickness * math::line_normal(math::fast::normalize(v));
            auto antinormal = -normal;
            auto middle = anchor.point - v * 0.5f; // required for transition from the 2-point segment to the 3-point (to avoid junction at the line cap)

            auto divisions = math::ceil<int>(cap.division_coeff * thickness);

            output.vertices.push_back({middle + normal, {start_offset + offset * 0.5f, +1.0f, thickness}});         // +0
            output.vertices.push_back({middle + antinormal, {start_offset + offset * 0.5f, -1.0f, thickness}});     // +1
            output.vertices.push_back({anchor.point + normal, {anchor.offset, 1.0f, thickness}});                  // +2
            output.vertices.push_back({anchor.point, {anchor.offset, 0.0f, thickness}});                           // +3
            output.vertices.push_back({anchor.point + antinormal, {anchor.offset, -1.0f, thickness}});             // +4

// prev to middle
            output.indices.push_back(anchor.vertices_offset - 2);
            output.indices.push_back(anchor.vertices_offset + 1);
            output.indices.push_back(anchor.vertices_offset - 1);

            output.indices.push_back(anchor.vertices_offset - 2);
            output.indices.push_back(anchor.vertices_offset + 0);
            output.indices.push_back(anchor.vertices_offset + 1);

// middle to cap
            output.indices.push_back(anchor.vertices_offset + 0);
            output.indices.push_back(anchor.vertices_offset + 2);
            output.indices.push_back(anchor.vertices_offset + 3);

            output.indices.push_back(anchor.vertices_offset + 0);
            output.indices.push_back(anchor.vertices_offset + 3);
            output.indices.push_back(anchor.vertices_offset + 1);

            output.indices.push_back(anchor.vertices_offset + 3);
            output.indices.push_back(anchor.vertices_offset + 4);
            output.indices.push_back(anchor.vertices_offset + 1);

//  cap
            auto arc_start_index = anchor.vertices_offset + 4;
            
            for (int i = 0; i < divisions; ++i) {
                output.vertices.push_back({anchor.point + math::fast::rotate(antinormal, math::pi * float(i + 1) / (divisions + 1)), {anchor.offset, -1.0f, thickness}});

                output.indices.push_back(arc_start_index - 1);
                output.indices.push_back(arc_start_index + i + 1);
                output.indices.push_back(arc_start_index + i);
            }

            output.vertices.push_back({anchor.point - normal, {anchor.offset, +1.0f, thickness}});
            output.vertices.push_back({anchor.point + normal, {anchor.offset, -1.0f, thickness}});

            output.indices.push_back(arc_start_index - 1);
            output.indices.push_back(arc_start_index + divisions + 2);
            output.indices.push_back(arc_start_index + divisions);
        }

        template <class T, class Model, class It>
        void append_line(line_join_miter, line_mesh<T, Model> & output, It it, It end, float thickness) {
            auto prev = it++;
            
            for (auto next = std::next(it); next != end; prev = std::exchange(it, next++)) {
                auto & point = it->point;
                auto v = point - prev->point;
                auto a = math::normalize(v);
                auto b = math::normalize(point - next->point);
                auto dot = math::dot(a, b);
                auto length = math::length(v);

                if (dot > 0.8f) {
                    auto normal = math::line_normal(a);
                    append_section(output, *it, normal, prev->offset + length, thickness);
                    append_section(output, *it, -normal, prev->offset + length, thickness);
                } else if (dot < -0.9f) {
                    append_section(output, *it, math::line_normal(a), prev->offset + length, thickness);
                } else {
                    append_section(output, *it, math::tangent_normal(a, -b), prev->offset + length, thickness);
                }
            }
        }

        template <class T, class Model, class It>
        void append_line(line_join_bevel, line_mesh<T, Model> & output, It it, It end, float thickness) {
            auto prev = it++;
            
            for (auto next = std::next(it); next != end; prev = std::exchange(it, next++)) {
                auto & point = it->point;
                auto v = point - prev->point;
                auto a = math::normalize(v);
                auto b = math::normalize(point - next->point);
                auto dot = math::dot(a, b);

                if (dot < -0.9f) {
                    append_section(output, *it, math::line_normal(a), prev->offset + math::length(v), thickness);
                } else {
                    append_bevel(output, *it, a, b, dot, prev->offset + math::length(v), thickness);
                }
            }
        }

        template <class T, class Model, class It>
        void append_line(const line_join_round & join, line_mesh<T, Model> & output, It it, It end, float thickness) {
            auto prev = it++;
            
            for (auto next = std::next(it); next != end; prev = std::exchange(it, next++)) {
                auto & anchor = *it;
                auto v = anchor.point - prev->point;
                auto a = math::normalize(v);
                auto b = math::normalize(anchor.point - next->point);
                auto angle = math::signed_angle(a, b);
                auto abs_angle = math::abs(angle);
                auto normal = math::line_normal(a);

                if (abs_angle >= math::pi * 0.95) {
                    append_section(output, *it, normal, prev->offset + math::length(v), thickness);
                } else {
                    anchor.vertices_offset = output.vertices.size();
                    anchor.indices_offset = output.indices.size();
                    anchor.offset = prev->offset + math::length(v);

                    auto divisions = math::ceil<int>(join.division_coeff * thickness * abs_angle / math::pi);
                    auto n = math::tangent_normal(a, -b);
            
                    if (angle > 0.0f) {
                        output.vertices.push_back({anchor.point + n * thickness, {anchor.offset, -1.0f, thickness}});   // +0
                        output.vertices.push_back({anchor.point, {anchor.offset, 0.0f, thickness}});                    // +1

    // prev to arc start
                        output.indices.push_back(anchor.vertices_offset - 2);
                        output.indices.push_back(anchor.vertices_offset + 2); // arc start
                        output.indices.push_back(anchor.vertices_offset - 1);

                        output.indices.push_back(anchor.vertices_offset - 2);
                        output.indices.push_back(anchor.vertices_offset + 0);
                        output.indices.push_back(anchor.vertices_offset + 2); // arc start

    // arc side triangle
                        output.indices.push_back(anchor.vertices_offset + 0);
                        output.indices.push_back(anchor.vertices_offset + 1);
                        output.indices.push_back(anchor.vertices_offset + 2); // arc start

                        auto arc_start_index = anchor.vertices_offset + 2;

                        for (int i = 0; i < divisions; ++i) {
                            output.vertices.push_back({anchor.point + math::fast::rotate(normal, angle * float(i + 1) / (divisions + 1)), {anchor.offset, +1.0f, thickness}});

                            output.indices.push_back(arc_start_index - 1);
                            output.indices.push_back(arc_start_index + i + 1);
                            output.indices.push_back(arc_start_index + i);
                        }

                        output.indices.push_back(arc_start_index - 1);
                        output.indices.push_back(arc_start_index + divisions + 2);
                        output.indices.push_back(arc_start_index + divisions);
                    }
                }
            }
        }
    }
}

//---------------------------------------------------------------------------
#endif
