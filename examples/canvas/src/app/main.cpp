//---------------------------------------------------------------------------

#include <launch/main.h>
#include <flow/context.h>
#include <flow/timer.h>

#include <scene/camera.h>

#include <app/gui.h>

#include <fs/fs.h>
#include <gfx3d_templates/mesh_quad.h>
#include <uio/mouse_navigator.h>

#include <ui/renderer.h>
#include <ui/widgets/box.h>
#include <ui/controllers/root_controller.h>

#include <spdlog/spdlog.h>
#include <fmt/format.h>
#include <math/io.h>

#include <app/widgets/canvas.h>
#include <app/widgets/line.h>
#include <app/controllers/draw_controller.h>

//---------------------------------------------------------------------------

namespace asd
{
    namespace app
    {
        using namespace std::chrono_literals;
        using namespace color::literals;
        using namespace space::literals;

        constexpr color::linear_rgb clear_color = 0x000_rgb;

        struct graphics
        {
            sdl::graphics & context;
            gfx::uniform_registry<opengl::graphics> & uniforms;
            gfx::shader_registry<opengl::graphics> & shaders;
            gfx::render_pass_registry<opengl::graphics> & passes;
            gfx::pipeline_registry<opengl::graphics> & pipelines;
        };

        static constexpr auto Resolution = gfx::create_uniform_layout("Resolution",
            gfx::uniforms::element<space::vector>{"resolution"}
        );

        static constexpr auto Time = gfx::create_uniform_layout("Time",
            gfx::uniforms::element<space::real>{"time"}
        );

        static constexpr auto Navigation = gfx::create_uniform_layout("Navigation",
            gfx::uniforms::element<space::point>{"panning"},
            gfx::uniforms::element<space::real>{"zoom"}
        );

        auto create_viewport_root = [](auto & render_context) {
            using line_type = gfx::line<float, math::point_model_xy>;

            // constexpr color::hsv base_color(0xd24329_rgb);

            return app::canvas(
                s::controller = app::draw_controller([&]() {
                    return app::line(
                        s::render_context = render_context,
                        s::line_type = asd::type_v<line_type>
                    );
                })
            );
        };

        using ui_environment = ui::default_environment<opengl::graphics>;
        using ui_render_context = ui::default_render_context<ui_environment>;

        struct view
        {
            using quads = gfx::mesh_quad_factory<gfx::vertex_layouts::p2t2>;

            app::gui & gui;
            uio::mouse_input & mouse;
            sdl::window & window;
            ui_render_context render_context;
            math::int_rect viewport {0, 0, 640, 640};
            scene::camera camera;
            scene::camera_view<opengl::graphics> camera_view;
            uio::mouse_navigator mouse_navigator;
            gfx::uniform<opengl::graphics, space::vector> resolution_uniform;
            gfx::uniform<opengl::graphics, space::real> time_uniform;
            gfx::uniform<opengl::graphics, space::point, space::real> navigation_uniform;
            opengl::shader_registry & shaders;
            gfx::handle<gfx::render_target<opengl::graphics>> render_target;
            gfx::texture<opengl::graphics> & viewport_texture;
            gfx::handle<gfx::render_pass<opengl::graphics>> main_pass;
            gfx::handle<gfx::render_pass<opengl::graphics>> viewport_pass;
            gfx::handle<gfx::mesh<opengl::graphics>> quad_mesh;

            ui::widgets::box root{create_viewport_root(render_context)};
            ui::controllers::root_controller<ui::widgets::box> controller{root};

            std::unique_ptr<SDL_Cursor, void(*)(SDL_Cursor *)> arrow_cursor{SDL_CreateSystemCursor(SDL_SYSTEM_CURSOR_ARROW), SDL_FreeCursor};
            std::unique_ptr<SDL_Cursor, void(*)(SDL_Cursor *)> hand_cursor{SDL_CreateSystemCursor(SDL_SYSTEM_CURSOR_HAND), SDL_FreeCursor};

            view(app::gui & gui, uio::mouse_input & mouse, sdl::window & window, ui_environment & environment) :
                gui(gui),
                mouse(mouse),
                window(window),
                render_context(environment),
                camera_view(environment.uniforms),
                mouse_navigator(mouse),
                resolution_uniform(environment.uniforms.create(Resolution)),
                time_uniform(environment.uniforms.create(Time)),
                navigation_uniform(environment.uniforms.create(Navigation)),
                shaders(environment.shaders),
                render_target(environment.render_targets.create({math::uint_size(viewport.size())})),
                viewport_texture(render_target->get_buffer(0)),
                main_pass(environment.passes.create({{}, clear_color})),
                viewport_pass(environment.passes.create({*render_target, {}, 0xff0_rgb})),
                quad_mesh(quads::create(environment.meshes))
            {
                camera.set_projection(scene::projection::screen);
                camera.set_far_distance(2.0_x);
                camera.set_position({0.0_x, 0.0_x, -1.0_x});
                camera.set_direction({0.0_x, 0.0_x, 1.0_x});

                mouse_navigator.set_zoom_speed(0.05f);

                this->mouse.move_input().subscribe([this](const uio::mouse_move_event & event) {
                    if (this->controller.move(space::point(event.mouse.position() - viewport.pos()))) {
                        SDL_SetCursor(hand_cursor.get());
                    } else {
                        SDL_SetCursor(arrow_cursor.get());
                    }
                });

                this->mouse.press_input().subscribe([this](const uio::mouse_button_event & event) {
                    if (event.button != uio::mouse_button::left) {
                        return;
                    }

                    this->controller.press(space::point(event.mouse.position() - viewport.pos()));
                });

                this->mouse.release_input().subscribe([this](const uio::mouse_button_event & event) {
                    if (event.button != uio::mouse_button::left) {
                        return;
                    }

                    this->controller.release();
                });

                this->mouse.wheel_input().subscribe([this](const uio::mouse_wheel_event & event) {
                    this->controller.wheel(space::point(event.mouse.position() - viewport.pos()), space::point(event.delta));
                });
            }

            void update(float elapsed) {
                time_uniform.assign(elapsed);
                navigation_uniform.assign(mouse_navigator.panning() * 4.0f, mouse_navigator.zoom());
            }

            void render_viewport(const math::int_rect & v) {
                root.update(space::size {v.size()});

                if (viewport != v) {
                    viewport = v;

                    render_target->resize(math::uint_size(viewport.size()));
                    camera.set_viewport(viewport.size());
                    resolution_uniform.assign(space::vector{viewport.width(), viewport.height()});
                }

                camera_view.update(camera);

                gui.graphics.set_viewport(viewport.size());
                viewport_pass->start();
                camera_view.activate();
                resolution_uniform.activate();
                render_context.render();
                viewport_pass->end();
            }

            void render() {
                gui.graphics.set_viewport(window.physical_size());
                main_pass->start();
                gui.render();
                main_pass->end();
            }
        };

        struct container
        {
            container(app::gui & gui, app::view & view, sdl::graphics & graphics, opengl::shader_registry & shaders) :
                gui(gui), view(view), graphics(graphics), shaders(shaders)
            {
                flow::timer::start(flow, 16ms, [&]() {
                    if (!this->gui.app.process_events()) {
                        flow.stop();
                    }
                });

                flow::stopwatch_timer<std::chrono::milliseconds>::start(flow, 16ms, [&](const auto & elapsed) {
                    auto current_time = elapsed.count() / 1000.0f;

                    if (current_time >= last_time + 1.0f) {
                        fps = frames / (current_time - last_time);
                        last_time = current_time;
                        frames = 0;
                    }

                    ++frames;

                    this->view.update(current_time);

                    this->gui.update([this](ImGuiID dockspace_id) {
                        auto root_node = ImGui::DockBuilderGetNode(dockspace_id);

                        if (root_node->IsEmpty()) {
                            ImGuiID main_dock = dockspace_id;
                            ImGuiID bottom_dock = ImGui::DockBuilderSplitNode(main_dock, ImGuiDir_Down, 0.25f, NULL, &main_dock);
                            ImGuiID right_dock = ImGui::DockBuilderSplitNode(main_dock, ImGuiDir_Right, 0.25f, NULL, &main_dock);

                            ImGui::DockBuilderDockWindow("Viewport", main_dock);
                            ImGui::DockBuilderDockWindow("Toolbox", right_dock);
                            ImGui::DockBuilderDockWindow("Log", bottom_dock);
                            ImGui::DockBuilderFinish(dockspace_id);
                        }

                        ImGui::PushStyleVar(ImGuiStyleVar_WindowPadding, ImVec2(0, 0));

                        if (ImGui::Begin("Viewport")) {
                            auto min = space::point(ImGui::GetCursorPos());
                            auto max = space::point(ImGui::GetWindowContentRegionMax());

                            math::float_rect viewport{min, max};

                            if (ImGui::IsWindowHovered() && this->view.mouse.is_pressed(uio::mouse_button::left)) {
                                this->view.mouse_navigator.enable();
                            } else {
                                this->view.mouse_navigator.disable();
                            }

                            this->view.render_viewport(math::int_rect(viewport));

                            ImGui::Image((ImTextureID)(intptr_t)this->view.viewport_texture.id(), viewport.size(), ImVec2(0, 1), ImVec2(1, 0));
                        }
                        ImGui::End();
                        ImGui::PopStyleVar();

                        if (ImGui::Begin("Toolbox")) {
                            ImGui::PushItemWidth(-1.0f);

                            this->gui.text(fmt::format("Fps: {:.2f}", fps));

                            ImGui::PopItemWidth();
                        }
                        ImGui::End();
                    });

                    this->view.render();
                    this->graphics.present();
                });
            }

            int run() {
                flow.run();

                return 0;
            }

            app::gui & gui;
            app::view & view;
            sdl::graphics & graphics;
            opengl::shader_registry & shaders;
            flow::context flow;

            float last_time = 0.0f;
            int frames = 0;
            float fps = 0.0f;
        };

        auto window_module() {
            namespace di = boost::di;

#if defined(ASD_OPENGL_ES)
            opengl::configuration opengl_config{opengl::profile::es, 3, 0, 0}; // opengl es
#else
            opengl::configuration opengl_config{opengl::profile::core, 3, 3, 0}; // desktop opengl
#endif

            return di::make_injector(
                di::bind<opengl::configuration>().to(std::move(opengl_config)),
                di::bind<sdl::window_title>().to("canvas"),
                di::bind<sdl::window_size>().to(sdl::window_size{640, 640})
            );
        }

        auto root_module = boost::di::make_injector(
            sdl::module(),
            window_module()
        );

        container & c = root_module.create<container &>();

        static entrance open([]() {
            try {
                return c.run();
            } catch (const std::exception & e) {
                std::cerr << e.what() << std::endl;
                throw e;
            }
        });
    }
}

//---------------------------------------------------------------------------
