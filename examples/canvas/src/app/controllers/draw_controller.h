//---------------------------------------------------------------------------

#pragma once

#ifndef APP_CONTROLLERS_DRAW_CONTROLLER_H
#define APP_CONTROLLERS_DRAW_CONTROLLER_H

//---------------------------------------------------------------------------

#include <ui/controllers/bound_controller.h>
#include <app/widgets/drawing.h>
#include <gfx/line.h>

//---------------------------------------------------------------------------

namespace asd
{
    namespace app
    {
        namespace controllers
        {
            template <class T, class Factory>
            class draw_controller : public ui::bound_controller<T>
            {
            public:
                using widget_type = decltype(std::declval<Factory>()());

                draw_controller(Factory factory) : _factory(factory) {}

                void press(const space::point & pos) noexcept override final {
                    _current_drawing = &this->_target->add(_factory());
                    _prev_pos = pos;

                    auto & model = _current_drawing->model();
                    model.line.append(&pos, &pos + 1, _thickness, _join, _cap);
                    model.mark_changed();
                }

                void release() noexcept override final {
                    if (!_current_drawing) {
                        return;
                    }

                    _current_drawing = nullptr;
                }

                bool move(const space::point & pos) noexcept override final {
                    if (!_current_drawing) {
                        return false;
                    }

                    auto & model = _current_drawing->model();

                    if (model.line.can_place(pos, _thickness * 0.5f)) {
                        _prev_pos = pos;
                        model.line.append(&pos, &pos + 1, _thickness, _join, _cap);
                        model.mark_changed();
                    }

                    return true;
                }

            private:
                widget_type * _current_drawing = nullptr;
                space::point _prev_pos;
                float _thickness = 4.0f;
                gfx::line_join_miter _join{};
                gfx::line_cap_round _cap{};
                Factory _factory;
            };
        }

        static constexpr auto draw_controller = [](auto factory) {
            return [=](auto & canvas) {
                return std::make_unique<app::controllers::draw_controller<plaintype(canvas), plaintype(factory)>>(factory);
            };
        };
    }
}

//---------------------------------------------------------------------------
#endif
