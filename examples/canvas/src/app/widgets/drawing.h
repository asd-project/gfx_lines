//---------------------------------------------------------------------------

#pragma once

#ifndef APP_WIDGETS_DRAWING_H
#define APP_WIDGETS_DRAWING_H

//---------------------------------------------------------------------------

#include <ui/widgets/widget.h>

#include <ui/model.h>
#include <ui/renderer.h>

//---------------------------------------------------------------------------

namespace asd
{
    namespace app
    {
        namespace s
        {
            using namespace ui::s;
        }

        namespace widgets
        {
            class drawing : public ui::widgets::basic_widget
            {
            public:
                drawing() noexcept = default;
                drawing(drawing &&) noexcept = default;
            };
        }
    }
}

//---------------------------------------------------------------------------
#endif
