//---------------------------------------------------------------------------

#pragma once

#ifndef APP_WIDGETS_LINE_H
#define APP_WIDGETS_LINE_H

//---------------------------------------------------------------------------

#include <app/widgets/drawing.h>
#include <ui/layers/line_layer.h>
#include <color/preset.h>

//---------------------------------------------------------------------------

namespace asd
{
    namespace app
    {
        namespace s
        {
            define_symbol(line_type);
        }

        namespace widgets
        {
            template <class LineType>
            class line : public app::widgets::drawing
            {
            public:
                using line_type = LineType;
                using model_type = ui::model<ui::line_layer<LineType>>;

                line(ui::model_observer<model_type> & observer) :
                    _model(observer)
                {
                    _model.color = color::black;
                }

                line(line &&) noexcept = default;

                model_type & model() noexcept {
                    return _model;
                }

                void update(const space::rect & geometry) noexcept override {
                    if (!_model.is_changed() && _geometry == geometry) {
                        return;
                    }
                
                    app::widgets::drawing::update(geometry);
                    
                    _model.update(_geometry, 0.0f);
                }

            private:
                model_type _model;
            };
        }

        static constexpr struct
        {
            template <class... T, useif<
                symbol::matches<T...> (
                    s::render_context   [symbol::required][symbol::unique],
                    s::line_type        [symbol::required][symbol::unique]
                )
            >>
            auto operator()(T &&... options) const noexcept {
                using line_type = plain<symbol::named_type<s::type::line_type, T...>>;
                using widget_type = widgets::line<line_type>;
                static_assert(std::is_base_of_v<widgets::drawing, widget_type>, "Unsupported line type");

                auto [named, _] = symbol::collect(std::forward<T>(options)...);

                return widget_type(
                    named[s::render_context](asd::type_v<widget_type>)
                );
            }

        } line;
    }
}

//---------------------------------------------------------------------------
#endif
