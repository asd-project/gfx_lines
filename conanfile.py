import os

from conans import ConanFile, CMake

project_name = "gfx_lines"


class GfxLinesConan(ConanFile):
    name = "asd.%s" % project_name
    version = "0.0.1"
    license = "MIT"
    author = "bright-composite"
    url = "https://gitlab.com/asd-project/%s" % project_name
    description = "Line mesh generation utilities"
    topics = ("asd", project_name)
    generators = "cmake"
    exports_sources = "include*", "src*", "CMakeLists.txt", "asd.json"
    requires = (
        "asd.gfx/0.0.1@asd/testing",
    )

    def source(self):
        pass

    def build(self):
        pass

    def package(self):
        self.copy("*.h", dst="include", src="include")

    def package_info(self):
        self.cpp_info.libs = []
